Ansible for Server Compliance
=========

This repo contains roles and playbooks to demo Ansible for Server Compliance e.g. CIS. 

Scanning and Reporting
------------

I use Red Hat Satellite 6 to configure OpenSCAP integration with Red Hat Satellite. Satellite provides a central repository for OpenSCAP reports and gives an overall view on compliance for a given security policy.
